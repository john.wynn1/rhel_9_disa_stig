RHEL 9 DISA STIG
=========

This Ansible Role is responsible for applying the Red Hat Enterprise Linux 9 DISA STIG. This code will be applicable to Red Hat Enterprise Linux 9 and compatible derivatives such as Rocky Linux 9. This Ansible Role is written with infrastructure personnel in mind and not for Cyber Security personnel. Tags are kept to a minimum as compared to typical DISA STIG Ansible Roles out there. Individual Tasks are not written for each STIG ID with the STIG ID being the tag to call on it. The general rule is that when you see a DISA STIG Check in the checklist, assume the code is doing exactly what the Check is telling you to enforce. By default, this Role will apply ALL DISA STIG checks with some limitations. Partitioning is assumed to be done at provisioning time, for example. It is also assumed that the Ansible Operator is providing initial credentials. This Role can be applied with or without centralized authentication being configured on the target just as long as the Ansible Operator provides it.

Warnings
--------

Ensure the Ansible User you are using has `sudo` privileges and has a password to perform `sudo` commands or you will lock yourself out when applying this Role. Typically, this shouldn't be a problem however, if you are executing against Cloud instances where SSH key auth is your only form of authentication, ensure you disable any DISA STIGs related to `sudo` and privilege escalation.

Expectations
------------

You are expected to read and understand this documentation BEFORE using this Ansible Role. Like any other Ansible Role out there, if you just throw some Variables in a file and run the Ansible Role before understanding what the Role does, you will potentially break your systems or applications. Instructions on how to use Ansible are outside of the scope of documentation for this Ansible Role. Some things you may need to be aware of are Ansible precedence, configuring authentication to your targets, building inventory files or relying on dynamic inventories for targeting, Ansible connection types and requirements, setting up your Ansible environment, etc... We provide an example of the most basic Ansible Playbook to get you up and running with this Ansible Role, anything more is outside of the scope of this documentation. We also do not go into great detail about "extra" options or why and why you shouldn't apply certain DISA STIG controls. It is up to the Ansible Operator to understand the requirements for their systems and configure the Variables as such.

Requirements
------------

If you are deploying this Role onto Red Hat based distros that do not have access to McAfee Threat Prevention toolsets, you must enable EPEL to install the ClamAV package set. This Role was tested with the below package versions.

| Software | Version |
| -------- | ------- |
| Ansible Core | 2.15.* |
| Ansible Lint | 24.2.* |
| Python | 3.11 |

Dependencies
------------

This Ansible Role primarily relies on the builtin Ansible Collection. Other Ansible Collections and their respective tested versions are listed below. There are no additional Ansible Role requirements to apply this Role.

| Collection | Version |
| ---------- | ------- |
| ansible.posix | 1.5.4 |
| community.general | 7.5.1 |

Available Tags
--------------

To keep Tags to a minimum, a Tag has been created to run each Task Group that match the name of the file. These Tags also match the corresponding Variables listed. There are some Tags that should be used together depending on if you are applying a new state to your endpoints. One example is that if you are installing a package required by the DISA STIG that you didn't have installed before and that package comes with its own Daemon, then after you change the package variable to `"yes"`, you should probably not only run the `packages` Tag but also the `configure_daemons` Tag as well so it starts and enables the service. This will allow for running your command once and knocking out more than one DISA STIG Check at a time. It is up to the Ansible Operator to understand the target system and what they are trying to do. Building every possible scenario of Tag combinations is outside the scope of Ansible Role documentation.

| Tag | Purpose | Example Execution | Comment |
| --- | ------- | ----------------- | ------- |
| `access_control` | Applies access control related configurations. | `ansible-playbook my-playbook.yml --tags "access_control"` | The variables associated with this Tag mostly control user sessions, password maintenance, lockouts, password quality, etc...
| `banners` | Applies the banner to the system. | `ansible-playbook my-playbook.yml --tags "banners"` | N/A
| `boot_process` | Applies boot proces configurations. | `ansible-playbook my-playbook.yml --tags "boot_process"` | The variables associated with this Tag consist of Grub and Kernel configurations. **This Tag causes a reboot of the system.**
| `configure_auditing` | Applies system auditing related configurations. | `ansible-playbook my-playbook.yml --tags "configure_auditing"` | The variables associated with this Tag include logging and log server, Auditd actions, log format, etc...
| `configure_cac` | Applies the OpenSC service configurations. | `ansible-playbook my-playbook.yml --tags "configure_cac"` | The variables associated with this Tag are strictly related to OpenSC.
| `configure_crypto` | Applies FIPS related configurations. | `ansible-playbook my-playbook.yml --tags "configure_crypto"` | The variables associated with this Tag are strictly related to FIPS. **This Tag causes a reboot of the system.**
| `configure_daemons` | Applies service related configurations. | `ansible-playbook my-playbook.yml --tags "configure_daemons"` | The variables associated with this Tag are mostly ensuring that any system service is enabled or disable based on the define variable and in some instances will initiate system service configuration changes.
| `configure_files` | Applies file related configurations. | `ansible-playbook my-playbook.yml --tags "configure_files"` | There are no user related variables associated to this Tag. This Tag ensures that all files and directories mentioned in the DISA STIG Checklist have the appropriate user and group ownership as well as permissions.
| `configure_firewall` | Applies firewall related configurations. | `ansible-playbook my-playbook.yml --tags "configure_firewall"` | The variables associated with this Tag allow the user to open port and/or services using Firewalld. These variables a blank by default and the code ensures that the `ssh` Service stays open however, all other inbound traffic is denied by default unless initiated from the system itself.
| `configure_limits` | Applies system login limit configurations. | `ansible-playbook my-playbook.yml --tags "configure_limits"` | N/A
| `configure_network` | Applies network related configurations. | `ansible-playbook my-playbook.yml --tags "configure_network"` | The variables associated with this Tag pertain to NetworkManager configurations as well as wireless capabilities.
| `configure_pamd` | Applies PAM related configurations. | `ansible-playbook my-playbook.yml --tags "configure_pamd"` | The variables associated with this Tag primarily configure PAM's authentication mechanisms to the target system.
| `configure_shell` | Applies Shell related configurations. | `ansible-playbook my-playbook.yml --tags "configure_shell"` | The variables associated with this Tag pertain to Tmux, Bash, C Shell, and Shell timeouts.
| `configure_ssh` | Applies SSH related configurations. | `ansible-playbook my-playbook.yml --tags "configure_ssh"` | The variables associated with this Tag configure various aspects of SSHD.
| `configure_sysctl` | Applies Sysctl related configurations. | `ansible-playbook my-playbook.yml --tags "configure_sysctl"` | N/A
| `configure_usbguard` | Applies USBGuard related configurations. | `ansible-playbook my-playbook.yml --tags "configure_usbguard"` | There are no user related variables associated to this Tag. This Tag sets the USBGuard Daemon backend and generates an initial USBGuard policy when the `usbguard` package exists on the endpoint.
| `gpg_signatures` | Applies repository related GPG configurations. | `ansible-playbook my-playbook.yml --tags "gpg_signatures"` | N/A
| `graphical_environment` | Applies desktop environment related configurations. | `ansible-playbook my-playbook.yml --tags "graphical_environment"` | The variables associated with this Tag specify whether you are running the server in "headless mode" or not and will apply extra configurations if you enable the desktop environment.
| `kernel_blacklist` | Applies Kernel module blacklisting configurations. | `ansible-playbook my-playbook.yml --tags "kernel_blacklist"` | N/A
| `mount_points` | Applies the `/dev/shm` mount point related configurations. | `ansible-playbook my-playbook.yml --tags "mount_points"` | There are no user related variables associated to this Tag. **This Tag causes a reboot of the system.**
| `packages` | Installs or uninstalls packages. | `ansible-playbook my-playbook.yml --tags "packages"` | The variables associated with this Tag performs a full system update on all packages, installs all required packages, and uninstalls all prohibited packages.
| `threat_prevention` | Applies threat prevention related configurations. | `ansible-playbook my-playbook.yml --tags "threat_prevention"` | The variables associated with this Tag deploy McAfee, ClamAV, AIDE, configure SELinux modes, etc... Note that you can install both McAfee and ClamAV at the same time and the code will not break, the system on the other hand may end up having issues so it is recommended to install one or the other.

Access Control Variables
------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `enable_local_fail_delay` | Enable failed login delays for PAMD. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `local_fail_delay_count` | The number in seconds of the login delay for PAMD. | `"4"` | Value must be a string. Only required if `enable_local_fail_delay` is set to `"yes"`.
| | `configure_local_default_umask` | Customize the default local umask for PAMD. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `local_default_umask` | The default local umask for PAMD. | `"077"` | Value must be a string. Only required if `configure_local_default_umask` is set to `"yes"`.
| | `enable_local_max_pass` | Enables the customization of maximum password lifetime on the system. | `"yes"` | Value must be a string. If you set this parameter to `"no"`, it will set the maximum password lifetime to `99999` days which is a system default. Valid options are `"yes"` and `"no"`.
| | `local_max_pass` | Sets the maximum password lifetime to specified number. | `"60"` | Value must be a string. Only required if `enable_local_max_pass` is set to `"yes"` and your organizational policy mandates a different maximum password lifetime than the default.
| | `enable_local_pass_lifetime` | Enable the local user password minimum lifetime. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `local_min_pass_lifetime` | The minimum lifetime of passwords for local user accounts. | `"1"` | Value must be a string. Only required if `enable_local_pass_lifetime` is set to `"yes"`.
| | `enable_local_pass_min_len` | Enable the minimum password length for the system in the login.defs file. | `"yes"` | Value must be a string. This function is unsupported by Linux but required by DISA STIGs so we configure it. Valid options are `"yes"` and `"no"`.
| | `local_pass_min_len` | Sets the minimum password lenght for the system in the login.defs file. | `"15"` | Value must be a string.his function is unsupported by Linux but required by DISA STIGs so we configure it. This can be changed based on organizational requirements and is only required when `enable_local_pass_min_len` is set to `"yes"`.
| | `configure_local_encrypt_method` | Customize the default password encryption algorithm for passwords stored on the system. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `local_encrypt_method` | The algorithm to use for encrypting passwords stored on the system. | `"SHA512"` | Value must be a string. Valid options are `"SHA512"`, `"SHA256"`, `"MD5"`, `"BLOWFISH"`, and `"DES"`. Only required if `configure_local_encrypt_method` is set to `"yes"`.
| | `enable_local_hash_rounds` | Enable number of rounds used by the SHA encryption method used in the `local_encrypt_method` variable. | `"yes"` | Value must be a string. Only valid when the `local_encrypt_method` is set to a `"SHA*"` based algorithm. Valid options are `"yes"` and `"no"`.
| | `local_hash_rounds_count` | The number of rounds used by the `local_encrypt_method` when set to a `"SHA*"` based algorithm. | `"5000"` | Value must be a string.
| | `enable_local_create_home` | Enable the creation of user home directories when local user is created. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_local_account_disablement` | Enable account disablement by the system. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `account_inactivity` | Time in days before the system disables user accounts. | `"35"` | Value must be a string. Required when `enable_local_account_disablement` is set to `"yes"`.
| | `configure_faillock` | Determine whether or not to configure Faillock. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_faillock_persist` | Configure account lockout across reboots. | `"yes"` | Value must be a string. Required when `configure_faillock` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `faillock_persist_file` | The directory where user files with the failure records are kept. | `"/var/log/faillock"` | Value must be a string. Required when `enable_faillock_persist` is set to `"yes"`.
| | `enable_faillock_user_audit` | Enable the logging of the Username when authenticating to the system in the system's log file. | `"yes"` | Value must be a string. Required when `configure_faillock` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `enable_local_account_lockout` | Configure Faillock to deny access upon consecutive failed login attempts. | `"yes"` | Value must be a string. Required when `configure_faillock` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `failed_login_attempts` | Set the number of consecutive failed login attempts before account lockout. | `"3"` | Value must be a string. Required when `enable_local_account_lockout` is set to `"yes"`.
| | `enable_faillock_interval` | Enable custom configuration of the Faillock interval of failed login attempts. | `"yes"` | Value must be a string. Required when `configure_faillock` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `faillock_interval` | Set the time in seconds of the Faillock interval. | `"900"` | Value must be a string. Required when `enable_faillock_interval` is set to `"yes"`.
| | `enable_faillock_unlock_time` | Enable custom configuration of the Faillock unlock time for locked accounts. | `"yes"` | Value must be a string. Required when `configure_faillock` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `faillock_unlock_time` | Set the time in seconds before Faillock automatically unlocks locked accounts. | `"0"` | Value must be a string. Required when `enable_faillock_unlock_time` is set to `"yes"`.
| | `even_deny_root` | Enable Faillock's ability to even lock the `root` user account. | `"yes"` | Value must be a string. Required when `configure_faillock` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `enable_system_default_profile_umask` | Determine whether the system default profile umask should be configured or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `system_default_profile_umask` | The system default profile umask. | `"077"` | Value must be a string. This can be changed based on organizational requirements. Only necessary if changing the default value provided.
| | `enable_stop_idle_session` | Enable terminating user sessions due to inactivity. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `stop_idle_session_secs` | Number of seconds before terminating a user session due to inactivity. | `"900"` | Value must be a string. Only required if `enable_stop_idle_session` is set to `"yes"`.
| | `configure_sudoers_timeout` | Determine whether to configure Sudo timeout or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sudoers_timeout` | Time in seconds for Sudo to reuse previous successful privilege escalation. | `"0"` | Value must be a string. This can be changed based on organizational policy or technology requirements. Leaving this configured as `"0"` forces the user to type their password for every "sudo" command typed in.
| | `configure_sudoers_user_password` | Determine whether to require password for `root` user or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `configure_sudo` | Determine whether to remove !authenticate from sudoers files. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_pwquality_new_characters` | Enable the checking of passwords for new characters via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_new_characters_count` | Set the number of new characters required in new passwords via pwquality. | `"8"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_new_characters` is set to `"yes"`.
| | `enable_pwquality_minlength` | Enable minimum password length via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_minlen` | Set the minimum number of characters required for passwords via pwquality. | `"15"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_minlength` is set to `"yes"`.
| | `enable_pwquality_numeric_requirement` | Enable numerical character password requirement via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_numeric_count` | Set the amount of numbers required in password via pwquality. | `"-1"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_numeric_requirement` is set to `"yes"`.
| | `enable_pwquality_uppercase_requirement` | Enable uppercase character password requirement via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_uppercase_count` | Set the number of required uppercase characters in passwords via pwquality. | `"-1"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_uppercase_requirement` is set to `"yes"`.
| | `enable_pwquality_lowercase_requirement` | Enable lowercase character password requirement via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_lowercase_count` | Set the amount of required lowercase characters in password via pwquality. | `"-1"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_lowercase_requirement` is set to `"yes"`.
| | `enable_pwquality_special_chars` | Enable special character requirements for passwords via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_special_chars_count` | Set the number of special characters required for passwords via pwquality. | `"-1"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_special_chars` is set to `"yes"`.
| | `enable_pwquality_minclass` | Enable the minimum number of character classes required in passwords via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_minclass_count` | Set the minimum number of character classes required in passwords via pwquality. | `"4"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_minclass` is set to `"yes"`.
| | `enable_pwquality_maxrepeat` | Enable the checking of passwords for repeated characters via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_maxrepeat_count` | Set the number of repeated characters allowed via pwquality. | `"3"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_maxrepeat` is set to `"yes"`.
| | `enable_pwquality_class_repeat` | Enable the checking of passwords for repeated character classes via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pwquality_class_repeat_count` | Set the number of repeated character classes allowed via pwquality. | `"4"` | Value must be a string. This can be changed based on organizational requirements and is only required when `enable_pwquality_class_repeat` is set to `"yes"`.
| | `enable_pwquality_dict_check` | Enable the checking of passwords for dictionary words via pwquality. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_root_password_complexity` | Enable password complexity rules for `root` user. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `user_group_checksum_algorithm` | Set the checksum algorithm for user and group administration utilities. | `"sha512"` | Value must be a string. Valid options are `"des"`, `"md5"`, `"blowfish"`, `"sha256"`, and `"sha512"`. This can be changed based on organizational requirements.
| | `enable_sssd_smart_card` | Enable smart card authentication via SSSD. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `configure_cache_credentials` | Customize cached credentials on the system. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `cache_credential_days` | Number of days to cache credentials. | `"1"` | Value must be a string. Only required if `configure_cache_credentials` is set to `"yes"`.
| | `configure_sssd_ocsp_dgst` | Configure the SSSD OCSP digest. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sssd_ocsp_dgst` | Set the SSSD OCSP digest algorithm. | `"sha256"` | Value must be a string. Valid options are `"sha1"`, `"sha256"`, `"sha384"`, and `"sha512"`. Must be configured when `configure_sssd_ocsp_dgst` is set to `"yes"`.
| | `configure_auth_emergency_mode` | Enables authentication for Emergency Mode. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `configure_auth_single_user_mode` | Enables authentication for Single User Mode. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_kerberos_auth` | Disables Kerberos based authentication. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.

Banners Variable
----------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `issue_banner` | This Variable provides the content for the `/etc/issue` file. | The default is the DOD Banner provided by the DISA STIG | The content can be changed to suit the needs of the organization and/or device.

Example Banners Variable
---------------------

```yaml
---
issue_banner: |
  "You are accessing a U.S. Government (USG) Information System (IS) that is provided for USG-authorized use only.

  By using this IS (which includes any device attached to this IS), you consent to the following conditions:

  -The USG routinely intercepts and monitors communications on this IS for purposes including, but not limited to, penetration testing, COMSEC monitoring, network operations and defense, personnel misconduct (PM), law enforcement (LE), and counterintelligence (CI) investigations.

  -At any time, the USG may inspect and seize data stored on this IS.

  -Communications using, or data stored on, this IS are not private, are subject to routine monitoring, interception, and search, and may be disclosed or used for any USG-authorized purpose.

  -This IS includes security measures (e.g., authentication and access controls) to protect USG interests -- not for your personal benefit or privacy.

  -Notwithstanding the above, using this IS does not constitute consent to PM, LE or CI investigative searching or monitoring of the content of privileged communications, or work product, related to personal representation or services by attorneys, psychotherapists, or clergy, and their assistants. Such communications and work product are private and confidential. See User Agreement for details."
```

Boot Process Variables
----------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `set_grub2_password` | This Variable enables/disables the configuration of a Grub2 Password | `false` | Valid parameters are `true` and `false` boolean values.
| | `grub2_password` | This is the Grub2 password value | `changeme` | This value should be changed and managed through a secure means. This could be an Ansible Vault file, Hashicorp Vault secret, or through other similar technologies.
| | `disable_virtual_syscall` | Disable virtual system calls. | `"yes"` | Value must be a string.
| | `enable_poisoning` | Enables page_poisoning for the Kernel. | `"yes"` | Value must be a string.
| | `enable_clear_slub_slab` | Enables clearing of SLUB/SLAB objects for the Kernel. | `"yes"` | Value must be a string.
| | `enable_pti` | Enables page table isolation for the Kernel. | `"yes"` | Value must be a string.
| | `enable_global_audit` | Enables auditing as soon as the boot process start. | `"yes"` | Value must be a string.
| | `enable_audit_backlog_limit` | Enables audit backlog limit before starting auditd. | `"yes"` | Value must be a string.
| | `audit_backlog_limit` | Sets the audit backlog limit number. | `"8192"` | Value must be a string. Only required if `enable_audit_backlog_limit` set to `"yes"` and organizational policy mandates a different audit backlog limit.

Configure Auditing Variables
----------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `is_central_log_server` | Is the target system a Central Log Server? | `"no"` | Value must be a string. Setting this to `"yes"` will allow Rsyslog to receive logs from other devices. The DISA STIG requires this functionality to be disabled unless the target is a Log Server. Valid options are `"yes"` and `"no"`.
| | `enable_monitor_remote_access_logging` | Enable remote access monitoring via Rsyslog. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_cron_logging` | Enable Cron job logging via Syslog. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_offload_audit_records` | Enable audit record offloading via Syslog. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `customize_rsyslog` | Push Rsyslog custom configuration template file. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_rsyslog_auth` | Enable Rsyslog authentication to remote log server before offloading logs. | `"yes"` | Value must be a string. Required when `customize_rsyslog` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `enable_rsyslog_tls` | Enable the encryption of logs before transferring them onto a different system or media via Rsyslog. | `"yes"` | Value must be a string. Required when `customize_rsyslog` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `enable_rsyslog_gtls` | Enable the usages of the GTLS driver to encrypt the logs via Rsyslog. | `"yes"` | Value must be a string. Required when `customize_rsyslog` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `configure_rsyslog_log_server` | Configure the Remote Syslog server to send the logs to. | `"yes"` | Value must be a string. Required when `customize_rsyslog` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `central_log_server` | The Remote Syslog server IP address or FQDN. | `"changeme"` | Value must be a string. Required when `configure_rsyslog_log_server` is set to `"yes"`.
| | `central_log_server_port` | The Remote Syslog server port number that it is lisenting on for logs. | `"514"` | Value must be a string. Required when `configure_rsyslog_log_server` is set to `"yes"`.
| | `configure_auditd` | Determine whether or not to configure Auditd | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `configure_auditd_local_events` | Determine whether to configure auditing local events or not. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `configure_auditd_write_logs` | Determine whether to configure Auditd to write logs to local disk or not. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `configure_auditd_log_group` | Determine whether to configure the auditing log file group or not. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_log_group` | The auditing log file group. | `"root"` | Value must be a string. Must be configured when `configure_auditd_log_group` is set to `"yes"`. Valid options are group names, not their GUID.
| | `configure_auditd_log_format` | Determine whether to configure the Auditd log format or not. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_log_format` | The log format Auditd uses. | `"ENRICHED"` | Value must be a string. Must be configured when `configure_auditd_log_format` is set to `"yes"`. Valid options are `"ENRICHED"` and `"RAW"`.
| | `configure_auditd_freq` | Determine whether to configure the Auditd flush to disk frequency or not. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_freq` | The number of records Auditd maintaines before flushing its cache to disk. | `"100"` | Value must be a string. Must be configured when `configure_auditd_freq` is set to `"yes"`.
| | `configure_auditd_name_format` | Determine whether to configure how the system name is inserted into the audit logs or not. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_name_format` | How the system name is inserted into the audit logs. | `"hostname"` | Value must be a string. Must be configured when `configure_auditd_name_format` is set to `"yes"`. Valid options are `"none"`, `"hostname"`, `"fqd"`, `"numeric"`, and `"user"`.
| | `configure_auditd_max_log_file_action` | Determine whether to configure the Auditd max log file action or not. | `"yes"` |  Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_max_log_file_action` | The action Auditd will take when max log file size is reached. | `ROTATE` | Value must be a string. Must be configured when `configure_auditd_max_log_file_action` is set to `"yes"`. Valid options are `"IGNORE"`, `"SYSLOG"`, `"SUSPEND"`, `"ROTATE"`, and `"KEEP_LOGS"`.
| | `configure_auditd_disk_space_left` | Determine whether or not to configure Auditd disk space left percentage. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_disk_space_left` | The percentage of free space available before taking action. | `"25%"` | Value must be a string. Must be configured when `configure_auditd_disk_space_left` is set to `"yes"`.
| | `configure_auditd_space_left_action` | Determine whether or not to configure Auditd disk space left action. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_space_left_action` | The action Auditd takes when the disk space left percentage is reached. | `"email"` | Value must be a string. Must be configured when `configure_auditd_space_left_action` is set to `"yes"`. Valid options are `"halt"`, `"single"`, `"suspend"`, `"email"`, `"syslog"`, and `"ignore"`.
| | `configure_auditd_action_mail_acct` | Determine whether to configure account used for mail action or not. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_action_mail_acct` | The account used for Auditd mail actions. | `"root"` | Value must be a string. Must be configured when `configure_auditd_action_mail_acct` is set to `"yes"`. Valid option must be a valid email address if not using `"root"`.
| | `configure_auditd_admin_space_left` | Determine whether or not to configure Auditd administrative disk space left percentage. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_admin_space_left` | The percentage of free space available before taking administrative action. | `"5%"` | Value must be a string. Must be configured when `configure_auditd_admin_space_left` is set to `"yes"`.
| | `configure_auditd_admin_space_left_action` | Determine whether to configure the Auditd administrative action when disk space left reaches threshold. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_admin_space_left_action` | The administrative action Auditd takes when the disk space left percentage is reached. | `"single"` | Value must be a string. Must be configured when `configure_auditd_admin_space_left_action` is set to `"yes"`. Valid options are `"halt"`, `"single"`, `"suspend"`, `"email"`, `"syslog"`, and `"ignore"`.
| | `configure_auditd_disk_full_action` | Determine whether or not to configure Auditd disk full action. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_disk_full_action` | The Auditd system action to take upon disk space filling up. | `"HALT"` | Value must be a string. Must be configured when `configure_auditd_disk_full_action` is set to `"yes"`. Valid options are `"HALT"`, `"SINGLE"`, `"SUSPEND"`, `"SYSLOG"`, and `"IGNORE"`.
| | `configure_auditd_disk_error_action` | Determine whether or not to configure Auditd disk error action. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_disk_error_action` | The Auditd system action to take upon disk errors. | `"HALT"` | Value must be a string. Must be configured when `configure_auditd_disk_error_action` is set to `"yes"`. Valid options are `"HALT"`, `"SINGLE"`, `"SUSPEND"`, `"SYSLOG"`, and `"IGNORE"`.
| | `configure_auditd_overflow_action` | Determine whether to configure how Auditd reacts to internal queue overflowing. | `"yes"` | Value must be a string and is required when `configure_auditd` is set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `auditd_overflow_action` | Configure Auditd action to take when internal queue is overflowing. | `"SYSLOG"` | Value must be a string. Must be configured when `configure_auditd_overflow_action` is set to `"yes"`. Valid options are `"IGNORE"`, `"SYSLOG"`, `"SUSPEND"`, `"SINGLE"`, and `"HALT"`.
| | `configure_auditd_failure_mode` | Set audit failure mode to `kernel panic`. | `"yes"`| Value must be a string. If set to `"no"`, the audit failure mode will be left as the system default of `"syslog"`. Valid options are `"yes"` and `"no"`.
| | `audit_execve` | Audit the execution of the "execve" system call. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_mod_calls` | Audit the execution of the chmod, fchmod, and fchmodat system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_chown_calls` | Audit the execution of the chown, fchown, fchownat, and lchown system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_attr` | Audit the execution of the setxattr, fsetxattr, lsetxattr, removexattr, fremovexattr, and lremovexattr system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_umount_calls` | Audit the execution of umount system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_umount2_calls` | Audit successful and unsuccessful uses of umount2 system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_chacl` | Audit the execution of the chacl command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_setfacl` | Audit the execution of the setfacl command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_chcon` | Audit the execution of the chcon command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_semanage` | Audit the execution of the semanage command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_setfiles` | Audit the execution of the setfiles command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_setsebool` | Audit the execution of the setsebool command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_file_dir_name_link_actions` | Audit the execution of the rename, unlink, rmdir, renameat, and unlinkat system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_trunc_creat_open_calls` | Audit the execution of the  truncate, ftruncate, creat, open, openat, and open_by_handle_at system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_delete_calls` | Audit the execution of the delete_module system call. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_init_calls` | Audit the execution of the init_module and finit_module system calls. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_chage` | Audit the execution of the chage command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_chsh` | Audit the execution of the chsh command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_crontab` | Audit the execution of the crontab command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_gpasswd` | Audit the execution of the gpasswd command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_kmod` | Audit the execution of the kmod command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_newgrp` | Audit the execution of the newgrp command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_pam_timestamp` | Audit the execution of the pam_timestamp_check command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_passwd` | Audit the execution of the passwd command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_postdrop` | Audit the execution of the postdrop command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_postqueue` | Audit the execution of the postqueue command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_ssh_agent` | Audit the execution of the ssh-agent command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_ssh_keysign` | Audit the execution of the ssh-keysign command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_su` | Audit the execution of the su command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_sudo` | Audit the execution of the sudo command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_sudoedit` | Audit the execution of the sudoedit command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_unix_chkpwd` | Audit the execution of the unix_chkpwd command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_unix_update` | Audit the execution of the unix_update command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_userhelper` | Audit the execution of the userhelper command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_usermod` | Audit the execution of the usermod command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_mount` | Audit the execution of the mount command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_init` | Audit successful and unsuccessful uses of the init command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_poweroff` | Audit the execution of the poweroff command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_reboot` | Audit the execution of the reboot command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_shutdown` | Audit the execution of the shutdown command. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `audit_account_actions` | Generate audit records for all account creations, modifications, disabling, and termination events that affect user accounts. | `"yes"` | Value must be a string. Note this option applies auditing rules as presented in the DISA STIG Checklist. Valid options are `"yes"` and `"no"`.
| | `configure_auditd_immutable` | Configure the system to protect logon UIDs from unauthorized change. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.

Configure CAC Variables
-----------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `configure_opensc` | Determine whether to configure OpenSC or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `opensc_card_driver` | Specify which card driver to use. | `"cac"` | Value must be a string. Examples of valid options are `"cac"` and `"PIV-II"`. Valid options are dependent on the system. You can check available drivers by running the `opensc-tool --list-drivers` on the target system. Required when  `configure_opensc` is set to `"yes"`.

Configure Crypto Variables
--------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `fips_mode_state` | Set the FIPS mode. | `"enable"` | Value must be a string. Valid paremeters are `"enable"` and `"disable"`. Note that it is better to perform FIPS configurations when the system is provisioned and not after the fact.
| | `ipsec_installed` | Does the system have IPSEC installed? | `"no"` | This is only necessary to change if the system has IPSEC installed. Typical use cases are for VPN servers or if the system uses a VPN to connect to remote networks.
| | `enable_ipsec_fips` | Enable FIPS for IPSEC. | `"yes"` | This only applies when `ipsec_installed` is set to `"yes"` and you can apply FIPS on IPSEC connections. If, for some reason, you cannot enforce FIPS for IPSEC due to some technology related limitations, set this to `"no"`.
| | `bind_installed` | Does the system have BIND installed? | `"no"` | This is only necessary to change if the system has BIND installed. Typical use case are for DNS servers.
| | `enable_bind_fips` | Enable FIPS for IPSEC. | `"yes"` | This only applies when `bind_installed` is set to `"yes"` and you can apply FIPS to BIND. If, for some reason, you cannot enforce FIPS for IPSEC due to some technology related limitations, set this to `"no"`.

Configure Daemons Variables
---------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `disable_ctr_alt_del` | Disables and masks the Ctrl-Alt-Del daemon. | `"yes"` | Value must be a string.
| | `disable_debug_shell` | Disables and masks the debug shell daemon. | `"yes"` | Value must be a string.
| | `disable_coredump_daemon` | Disables the Coredump Daemon. | `"yes"` | Value must be a string. The only need to change this would be for troubleshooting Kernel level issue. If there is a requirement to do Kernel level debugging and development, you probably want this set to `"no"`.
| | `disable_kdump` | Disables and masks the Kdump daemon. | `"yes"` | Value must be a string. The only need to change this would be for troubleshooting Kernel level issue. If there is a requirement to do Kernel level debugging and development, you probably want this set to `"no"`.
| | `disable_automount` | Disables automounting external storage devices. | `"yes"` | Value must be a string. If there is a requirement for the system to mount things like USB drives, this should be set to `"no"`.
| | `enable_firewalld` | Enables and starts the Firewalld daemon. | `"yes"` | Value must be a string. Only applicable if the `firewalld` package is installed. Does not need to be changed or present if `firewalld` is not installed.
| | `enable_chronyd` | Enables and starts the Chronyd Daemon. | `"yes"` | Value must be a string. If the target is acting as an NTP server for your environment, this should be set to `"no"`.
| | `ntp_server` | The NTP server to use. | `"0.us.pool.ntp.mil"` | Value must be a string. This can be changed based on organizational policy and can be an FQDN or IP address. Multiple NTP servers are not yet supported.
| | `disable_chrony_server` | Disables the ChronyD Server functionality. | `"yes"` | Value must be a string. If the target is acting as an NTP server for your environment, this should be set to `"no"`.
| | `disable_chrony_network_management` | Disables the network management of Chronyd | `"yes"` | Value must be a string. If the target is acting as an NTP server for your environment, this should be set to `"no"`.
| | `enable_sshd` | Enables and starts the SSHD daemon. | `"yes"` | Value must be a string.
| | `enable_usbguard` | Enables and starts the usbguard daemon. | `"yes"` | Value must be a string. Only applicable if the `usbguard` package is installed. Does not need to be changed or present if `usbguard` is not installed.
| | `enable_fapolicyd` | Enables and starts the fapolicyd daemon. | `"yes"` | Value must be a string. Only applicable if the `fapolicyd` package is installed. Does not need to be changed or present if `fapolicyd` is not installed.
| | `enable_pcscd` | Enables and starts the PCSCD service. | `"yes"` | Value must be a string. This variable only applies when the `install_pcsc_lite` is set to `"yes"` and is only required if there is a requirement/desire to disable the PCSCD service.
| | `enable_rsyslog` | Enable the Rsyslog Daemon. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_audit` | Enable the Auditd Daemon. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_postfix` | Enables and starts the Postfix service. | `"yes"` | Value must be a string. This variable only applies when the `install_postfix` is set to `"yes"` and is only required if there is a requirement/desire to disable the Postfix service.

Configure Firewall Variables
----------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `firewall_services` | This Variable defines what FirewallD services to allow. | empty | These services can be defined by the Ansible Operator if a FirewallD profile exists and wishes to forego using port number/protocol syntax. The services defined here should match the FirewallD profiles and should be in a list if multiple services are desired. Variable type is string.
| | `firewall_ports` | This Variable defines what FirewallD port and protocol to allow. | empty | These ports/protocols can be defined by the Ansible Operator if it is desired to open ports/protocols on the firewall. The ports/protocols defined here should match the FirewallD syntax and be in a list if multiple ports/protocols are defined. Variable type is string.

Example Firewall Variables
--------------------------

```yaml
---
firewall_services:
  - http
  - https

firewall_ports:
  - 8080/tcp
  - 9090/tcp
  - 514/udp
```

Configure Limits Variables
--------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `enable_maxlogins` | Enables the maximum simultaneous sessions for a single user to a system. | `"yes"` | Value must be a string.
| | `maxlogins_count` | Sets maximum number of logins per user at any given time. | `"10"` | Value must be a string. The value can be changed based on organizational policy.

Configure Network Variables
---------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `networkmanager_dns_mode` | This Variable sets the NetworkManager DNS Mode. | `"none"` | Valid Options are `"none"` and `"default"`. Use `"none"` when NetworkManager is not configuring DNS. Use `"default"` when you allow NetworkManager to control DNS for the system.
| Required when `networkmanager_dns_mode` set to `"default"` | `dns_server` | This Variable defines the DNS server IPv4 address | empty | The Ansible Operator can define up to three DNS IPv4 IP addresses in a list.
| Required when `networkmanager_dns_mode` set to `"default"` | `search_domain` | This Variable defines the DNS search domains. | empty | The Ansible Operator can define DNS search domains in the form of a list.
| | `disable_wireless` | Disables all wireless radios. | `"yes"` | Value must be a string. If the target has wireless network cards and requires access to wireless access points, this should be set to `"no"`.

Example Network Variables
-------------------------

```yaml
---
dns_server:
  - "10.10.10.1"
  - "dns1.example.com"

search_domain:
  - "site1.example.com"
  - "site2.example.com"
```

Configure Pamd Variables
------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `enable_pam_password_retries` | Enables PAMD's ability to lock an account based off of failed authentication attempts. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pamd_password_auth_retries` | Set the number of authentication retries allowed by PAMD before locking the account. | `"3"` | Value must be a string. This can be changed based on ogranizational policy and is only set when `enable_pam_password_retries` is set to `"yes"`
| | `enable_pam_password_auth_reuse` | Enables PAMD's ability to remember passwords in the password-auth file. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pamd_password_auth_reuse` | Set the number of passwords remembered by PAMD in the password-auth file. | `"5"` | Value must be a string. This can be changed based on ogranizational policy and is only set when `enable_pam_password_auth_reuse` is set to `"yes"`.
| | `enable_pam_system_auth_reuse` | Enables PAMD's ability to remember passwords in the system-auth file. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pamd_system_auth_reuse` | Set number of passwords remember by PAMD in the system-auth file. | `"5"` | Value must be a string. This can be changed based on ogranizational policy and is only set when `enable_pam_system_auth_reuse` is set to `"yes"`.
| | `pamd_system_auth_retries` | Set number of retries allowed by PAMD in the system-auth file. | `"3"` | Value must be a string. This can be changed based on ogranizational policy and is only set when `enable_pam_system_auth_reuse` is set to `"yes"`.
| | `restrict_su_usage` | Determine whether to configure PAM to restrict the use of the "su" command or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `pamd_password_auth_hashing_rounds` | Set hashing rounds for PAMD in the password-auth file. | `"5000"` | Value must be a string. This can be changed based on ogranizational policy.
| | `pamd_system_auth_hashing_rounds` | Set hashing rounds for PAMD in the system-auth file. | `"5000"` | Value must be a string. This can be changed based on ogranizational policy.

Example PAM Variables
---------------------

```yaml
---
enable_pam_password_retries: "yes"
pamd_password_auth_retries: "3"
enable_pam_password_auth_reuse: "yes"
pamd_password_auth_reuse: "5"
enable_pam_system_auth_reuse: "yes"
pamd_system_auth_reuse: "5"
pamd_system_auth_retries: "3"
pamd_password_auth_hashing_rounds: "5000"
pamd_system_auth_hashing_rounds: "5000"
```

Configure Shell Variables
-------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `enable_tmux_session_lock` | Enables session locking of Tmux with vLock. | `"yes"` | Value must be a string. This variable only applies when `install_tmux` is set to `"yes"`.
| | `enable_tmux_inactivity_lock` | Enables session locking due to inactivity in Tmux. | `"yes"` | Value must be a string. This variable only applies when `install_tmux` is set to `"yes"`.
| | `tmux_inactivity_time` | The time in seconds of inactivity before Tmux session lock. | `"900"` | Value must be a string. Only required if `enable_tmux_inactivity_lock` is `"yes"` and organizational policy mandates a different Tmux inactivity timeout for sessions.
| | `enable_tmux_init` | Enables the automatic initialization of a Tmux session upon initiating a shell. | `"yes"` | Value must be a string. This variable only applies when `install_tmux` is set to `"yes"`.
| | `tmux_shell_script` | The shell script code to initialize the Tmux session. | <code>if [ "$PS1" ]; then parent=$(ps -o ppid= -p $$) name=$(ps -o comm= -p $parent) case "$name" in sshd&#124;login) tmux ;; esac fi</code> | This variable only applies when `enable_tmux_init` is set to `"yes" `. In general, this should not need changing unless you have a specific use case.
| | `remove_tmux_from_shells` | Removes Tmux from `/etc/shells` file on the system. | `"yes"` | Value must be a string. This variable only applies when `install_tmux` is set to `"yes"`.
| | `enable_shell_timeout` | Enables closing idle shell sessions. | `"yes"` | Value must be a string.
| | `shell_timeout_secs` | Configures the idle timeout time in seconds. | `"900"` | Value must be a string. The variable only applies when `enable_shell_timeout` is set to `"yes"` and only needs to be changed if the organizational policy is different than the default.
| | `configure_bashrc_umask` | Determine whether the bashrc umask should be configured or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `bashrc_umask` | The bashrc umask. | `"077"` | Value must be a string. This can be changed based on organizational requirements. Only necessary if changing the default value provided.
| | `configure_cshrc_umask` | Determine whether the cshrc umask should be configure or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `cshrc_umask` | The cshrc umask. | `"077"` | Value must be a string. This can be changed based on organizational requirements. Only necessary if changing the default value provided.

Example Tmux Script Variable
----------------------------

```yaml
tmux_shell_script: |-
  if [ "$PS1" ]; then
    parent=$(ps -o ppid= -p $$)
    name=$(ps -o comm= -p $parent)
    case "$name" in sshd|login) tmux ;; esac
  fi
```

Configure SSH Variables
-----------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `sshd.loglevel` | Set the SSHD logging level. | `"VERBOSE"` | Value must be a string. Valid options are `"QUIET"`, `"FATAL"`, `"ERROR"`, `"INFO"`, `"VERBOSE"`, `"DEBUG"`, `"DEBUG1"`, `"DEBUG2"`, and `"DEBUG3"`.
| | `sshd.keyauth` | Enables/Disables public key authentication for SSHD. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.permitemptypass` | Enables/Disables empty passwords for SSHD. | `"no"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.permitrootlogin` | Enables/Disables allowing `root` user authentication for SSHD. | `"no"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.usepam` | Enables/Disables PAMD for SSHD. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.hostbasedauth` | Enables/Disables local user authentication for SSHD. | `"no"` | Value must be a string. If the target is not joined to a Domain or pointed to an LDAP server where PAMD is being used for validating users, this should be set to `"yes"`. Valid options are `"yes"` and `"no"`.
| | `sshd.permituserenv` | Specifies whether ~/.ssh/environment and environment= options in ~/.ssh/authorized_keys are processed by SSHD. | `"no"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.rekeylimit` | Specifies the maximum amount of data and time that may be transmitted before the session key is renegotiated. | `"1G 1h"` | Value must be string. Amount of data and time can be adjusted based on organizational policy.
| | `sshd.clientalivecountmax` | Configures max client alive messages before disconnecting the connection. | `"1"` | Value must be a string. The value can be adjusted based on organizational policy or technology contraints.
| | `sshd.clientaliveinterval` | Configures the time in seconds of inactivity before disconnecting the connection. | `"600"` | Value must be a string. The value can be adjusted based on organizational policy.
| | `sshd.compression` | Enables/Disables session compression after authentication. | `"no"` | Value must be a string. Valid options are `"yes"`, `"delayed"`, and `"no"`.
| | `sshd.gssapiauth` | Enables/Disables authentication via GSSAPI. | `"no"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.kerberosauth` | Enables/Disables authentication via Kerberos. | `"no"` | Value must be a string. The value can be adjusted based on organizational policy. Valid options are `"yes"` and `"no"`.
| | `sshd.ignorerhosts` | Specifies whether to ignore per-user .rhosts and .shosts files during HostbasedAuthentication. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.ignoreuserknownhosts` | Specifies whether to ignore the user's ~/.ssh/known_hosts during HostbasedAuthentication and use only the system-wide known hosts file /etc/ssh/known_hosts. | `"yes"` | Value must be a string. This option may break applications like Ansible that rely on the knownhosts file. Valid options are `"yes"` and `"no"`.
| | `sshd.x11forwarding` | Enables/Disables X11 forwarding. | `"no"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.strictmodes` | Enables/Disables checking permissions and ownership of home directory before authenticating. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.printlastlog` | Enables/Disables printing last successfull login to user upon authentication. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `sshd.useprivseparation` | Set creating unpriviledged child processes to deal with incoming network traffic. | `"sandbox"` | Value must be a string. It is important to note that this parameter has been deprecated since OpenSSG 7.5. When it is officially removed, this variable will be removed from this Role. Valid options are `"sandbox"`, `"yes"`, and `"no"`.
| | `sshd.x11localhost` | Enable/Disable binding the X11 forwarding server to the loopback address or to the wildcard address. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
 | | `sshd.default_include_file` | The default SSHD include config file path. | `"/etc/ssh/sshd_config.d/50-redhat.conf"` | Value must be a string. This is the "include" file and not the `sshd_config.conf` file. We do this to avoid issues when replacing the `sshd_config.conf` during updates/upgrades to SSHD.
| | `sshd.ciphers` | Configures the ciphers authorized by SSHD. | `aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes128-gcm@openssh.com,aes128-ctr` | This value can be changed depending on organizational policy and technology constraints if any.
| | `sshd.macs` | Configures the MACS authorized by SSHD. | `hmac-sha2-256-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha2-256,hmac-sha1,umac-128@openssh.com,hmac-sha2-512` | This value can be changed depending on organizational policy and technology constraints if any.

Example Configure SSH Variables
-------------------------------

```yaml
sshd:
  loglevel: "VERBOSE"
  keyauth: "yes"
  permitemptypass: "no"
  permitrootlogin: "no"
  usepam: "yes"
  hostbasedauth: "no"
  permituserenv: "no"
  rekeylimit: "1G 1h"
  clientalivecountmax: "1"
  clientaliveinterval: "600"
  compression: "no"
  gssapiauth: "no"
  kerberosauth: "no"
  ignorerhosts: "yes"
  ignoreuserknownhosts: "yes"
  x11forwarding: "no"
  strictmodes: "yes"
  printlastlog: "yes"
  useprivseparation: "sandbox"
  x11localhost: "yes"
  default_include_file: "/etc/ssh/sshd_config.d/50-redhat.conf"
  ciphers: aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes128-gcm@openssh.com,aes128-ctr
  macs: hmac-sha2-256-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha2-256,hmac-sha1,umac-128@openssh.com,hmac-sha2-512
```

Configure Sysctl Variables
--------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `restrict_kernel_messages` | Restricted access to the Kernel message buffer. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `prevent_kernel_profiling` | Prevent kernel profiling by nonprivileged users. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `restrict_kernel_pointer_access` | Restrict exposed kernel pointer addresses access. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_hardlink_dac` | Enable kernel parameters to enforce discretionary access control on hardlinks. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_symlink_dac` | Enable kernel parameters to enforce discretionary access control on symlinks. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `prevent_kexec_load` | Prevent the loading of a new kernel for later execution. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_kernel_core_pattern` | Disable the kernel.core_pattern. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_aslr` | Implement address space layout randomization (ASLR). | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_unprivileged_bpf` | Disable access to network BPF system call from nonprivileged processes. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `restrict_ptrace` | Restrict usage of ptrace to descendant processes. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_user_namespaces` | Disable the use of user namespaces. | `"yes"` | Value must be a string. Note that if the host is using containers in any way, this should be set to `"no"`. Valid options are `"yes"` and `"no"`.
| | `enable_bpf_jit_harden` | Enable hardening for the Berkeley Packet Filter just-in-time compiler. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_ipv4_tcp_syncookies` | Enable the use of IPv4 TCP syncookies. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_ipv4_icmp_log_martian` | Enable the logging IPv4 packets with impossible addresses. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_ipv4_icmp_log_martian_default` | Enable the logging IPv4 packets with impossible addresses by default. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_ipv4_reverse_path_filter` | Enable the use reverse path filtering on all IPv4 interfaces. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_ipv4_reverse_path_filter_default` | Enable the use a reverse-path filter for IPv4 network traffic when possible by default. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_ipv4_icmp_echo_ignore_broadcast` | Enable the ignoring of IPv4 Internet Control Message Protocol (ICMP) echoes sent to a broadcast address. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `enable_ipv4_limit_bogus_icmp_errors` | Enable limiting the number of bogus IPv4 Internet Control Message Protocol (ICMP) response errors logs. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_ipv4_icmp_redirect_messages` | Disable IPv4 Internet Control Message Protocol (ICMP) redirect messages. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_ipv4_icmp_source_route` | Disable forwarding IPv4 source-routed packets. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_ipv4_icmp_redirect_default` | Disable IPv4 Internet Control Message Protocol (ICMP) redirect messaged from being accepted by default. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_ipv4_source_route_default` | Disable forwarding IPv4 source-routed packets by default. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_ipv4_icmp_redirects` | Disable the sending of IPv4 Internet Control Message Protocol (ICMP) redirects. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_ipv4_icmp_redirects_default` | Disable the ability to perform IPv4 Internet Control Message Protocol (ICMP) redirects by default. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `disable_ipv4_packet_forwarding` | Disable IPv4 packet forwarding. | `"yes"` | Value must be a string. Note that if the system is acting as a router, this should be set to `"no"`. Valid options are `"yes"` and `"no"`.
| | `disable_ipv6_accept_router_advertisement` | Disable the acceptance of IPv6 router advertisements on all interfaces. | `"yes"` | Value must be a string. Note that if the system is acting as a router or IPv6 is not enabled on the system this should be set to `"no"`. Valid options are `"yes"` and `"no"`.
| | `disable_ipv6_redirect_messages` | Disable IPv6 Internet Control Message Protocol (ICMP) redirect messages. | `"yes"` | Value must be a string. Note this should be set to `"no"` when IPv6 is not enabled on the system. Valid options are `"yes"` and `"no"`.
| | `disable_ipv6_source_route` | Disable the forwarding of IPv6 source-routed packets. | `"yes"` | Value must be a string. Note this should be set to `"no"` when IPv6 is not enabled on the system. Valid options are `"yes"` and `"no"`.
| | `disable_ipv6_packet_forwarding` | Disable IPv6 packet forwarding. | `"yes"` | Value must be a string. Note that if the system is acting as a router or IPv6 is not enabled on the system this should be set to `"no"`. Valid options are `"yes"` and `"no"`.
| | `disable_ipv6_router_advertisement_default` | Disable the acceptance of IPv6 router advertisements on all interfaces by default. | `"yes"` | Value must be a string. Note that if the system is acting as a router or IPv6 is not enabled on the system this should be set to `"no"`. Valid options are `"yes"` and `"no"`.
| | `disable_ipv6_icmp_redirect_messages` | Disable the acceptances of IPv6 Internet Control Message Protocol (ICMP) redirect messages. | `"yes"` | Value must be a string. Note this should be set to `"no"` when IPv6 is not enabled on the system. Valid options are `"yes"` and `"no"`.
| | `disable_ipv6_redirect_messages_default` | Disable the forwarding of IPv6 source-routed packets by default. | `"yes"` | Value must be a string. Note this should be set to `"no"` when IPv6 is not enabled on the system. Valid options are `"yes"` and `"no"`.

Configure USBGuard Variables
----------------------------
| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| No | `add_custom_usbguard_rules` | This Variable enables/disables the application of custom USBGuard Rules. | `"no"` | Valid parameters are `"yes"` and `"no"` | The typical use case for enabling this is if you have a KVM or docking station between your system and other USB devices.
| Only when `add_custom_usbguard_rules` is set to `"yes"` | `usbguard_custom_rules` | This Variable can be a list or blob of USBGuard rules. | Nothing is defined | Please review the USBGuard documentation to ensure correct syntax. The Variables must be formatted in the same manner as what is in the file or can also be in Ansible List format.

Example USBGuard Variables
--------------------------

```yaml
---
add_custom_usbguard_rules: "yes"
usbguard_custom_rules: |
  allow id 0000:0000 serial "0000000000" name "SCR33xx v2.0 USB SC Reader" via-port "0-00" with-interface 00:00:00 with-connect-type "hotplug"
  allow id 0000:0000 serial "" name "Secure KVM" via-port "0-0.0" with-interface { 00:00:00 00:00:00 } with-connect-type "unknown"
```

```yaml
---
add_custom_usbguard_rules: "yes"
usbguard_custom_rules:
  - allow id 0000:0000 serial "0000000000" name "SCR33xx v2.0 USB SC Reader" via-port "0-00" with-interface 00:00:00 with-connect-type "hotplug"
  - allow id 0000:0000 serial "" name "Secure KVM" via-port "0-0.0" with-interface { 00:00:00 00:00:00 } with-connect-type "unknown"
```

GPG Signatures Variables
------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `enable_repo_gpg_checking` | This Variable enables/disables the enablement of GPG checking for Repositories. | `"yes"` | Valid parameters are `"yes"` and `"no"` string values.

Graphical Environment Variables
-------------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `headless_server` | This Variable enables/disables the configuration requirements for the GUI. | `"yes"` | Valid parameters are `"yes"` and `"no"` string values.
| | `enable_smart_card` | Enables automatic locking of Gnome when Smart Card is removed from system. | `"yes"` | Value must be a string. This variable only applies when `headless_server` is set to `"no"`

Kernel Blacklist Variables
--------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `blacklist_atm` | Blacklists the Async Transfer Mode(ATM) Kernel module. | `"yes"` | Value must be a string.
| | `blacklist_can` | Blacklists the Controller Area Network(CAN) Kernel module. | `"yes"` | Value must be a string.
| | `blacklist_firewire` | Blacklists the FireWire Kernel module. | `"yes"` | Value must be a string.
| | `blacklist_sctp` | Blacklists the Stream Control Transmission Protocol(SCTP) Kernel module. | `"yes"` | Value must be a string.
| | `blacklist_tipc` | Blacklists the Transparent Inter Process Communication(TIPC) Kernel module. | `"yes"` | Value must be a string.
| | `blacklist_cramfs` | Blacklists the CramFS Kernel module. | `"yes"` | Value must be a string.
| | `blacklist_usb_storage` | Blacklists the usb-storage Kernel module. | `"yes"` | Value must be a string.
| | `blacklist_bluetooth` | Blacklists the Bluetooth Kernel module. | `"yes"` | Value must be a string.

Packages Variables
------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `upgrade_packages` | This Variable enables/disables the full upgrade of the target system packages. | `"no"` | Valid parameters are `"yes"` and `"no"` string values.
| | `install_subscription_manager` | Install the `subscription-manager` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_openssl` | Install the `openssl-pkcs11` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_gnutls` | Install the `gnutls-utils` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_nss_tools` | Install the `nss-tools` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_rng_tools` | Install the `rng-tools` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_s_nail` | Install the `s-nail` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_firewalld` | Install the `firewalld` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_chrony` | Install the `chrony` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_libreswan` | Install the `libreswan` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_openssh_server` | Install the `openssh-server` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_openssh_clients` | Install the `openssh-clients` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_usbguard` | Install the `usbguard` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_tmux` | Install the `tmux` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_policycoreutils` | Install the `policycoreutils` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_policycoreutils_python_utils` | Install the `policycoreutils-python-utils` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_sudo` | Install the `sudo` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_fapolicyd` | Install the `fapolicyd` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_pcsc_lite` | Install the `pcsc-lite` and `pcsc-tools` packages. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_opensc` | Install the `opensc` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_aide` | Install the `aide` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_rsyslog` | Install the `rsyslog` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_rsyslog_gnutls` | Install the `rsyslog-gnutls` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_audit` | Install the `audit` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_audispd_plugins` | Install the `audispd-plugins` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_postfix` | Install the `postfix` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `install_crypto_policies` | Install the `crypto-policies` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_vsftpd` | Uninstall the `vsftpd` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_sendmail` | Uninstall the `sendmail` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_nfs_utils` | Uninstall the `nfs_utils` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_ypserv` | Uninstall the `ypserv` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_rsh_server` Uninstall the `rsh-server` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_telnet` | Uninstall the `telnet-server` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_gssproxy` | Uninstall the `gssproxy` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_iprutils` | Uninstall the `iprutils` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_tuned` | Uninstall the `tuned` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_tftp` | Uninstall the `tftp` and `tftp-server` packages. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `uninstall_quagga` | Uninstall the `quagga` package. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.

Threat Prevention Variables
---------------------------

| Required | Variable Name | Function | Default value | Comment |
| -------- | ------------- | -------- | ------------ | ------- |
| | `deploy_mcafee` | This Variable deploys McAfee Threat Prevention to endpoints. | `"yes"` | Valid parameters are `"yes"` and `"no"` string values. This can be set to `"yes"` if `deploy_clamav` is set to `"no"`. This Variable assumes you have a Repository configured on the target that contains the McAfee packages.
| Required only when `deploy_mcafee` is `"yes"` and no repository is configure to pull the packages from. | `mcafee_threat_prevention.packages` | This Variable defines the package name to install on the endpoints. | `mcafeetp` | This Variable may be any compatible DNF package convention, ie web url, local package rpm, etc...
| Required only when `deploy_mcafee` is `"yes"` and no repository is configure to pull the packages from. | `mcafee_threat_prevention.daemon` | This Variable defines the Daemon name for the McAfee Threat Prevention toolset. | `mfetpd` | There should not be a need to change the default but is required to be defined if changing the `mcafee_threat_prevention.packages` Variable.
| | `deploy_clamav` | This Variable deploys ClamAV Threat Prevention to endpoints. | `"no"` | Valid parameters are `"yes"` and `"no"` string values. This can be set to `"yes"` if `deploy_mcafee` is set to `"no"`. This Variable assumes you have a Repository configure on the target that contains the ClamAV packages.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.packages` | This Variable defines the package name to install on the endpoints. | `- clamav, - clamd, - clamav-update` | This Variable may be any compatible DNF package convention, ie web url, local package rpm, etc... There should not be a need to change the default.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.daemon` | This Variable defines the Daemon names for the ClamAV services. | `- clamd@scan, - freshclam` | There should not be a need to change the default.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.freshclam.log_file` | This Variable defines the ClamAV log file. | `/var/log/freshclam.log` | There should not be a need to change the default.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.freshclam.db_owner` | This Variable defines the username ClamAV will use to interact with the local virus definition database. | `clamupdate` | Do not change this variable or this Role will fail.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.freshclam.private_mirror` | This Variable defines the virus definition repository for ClamAV to pull from. | `changeme.net` | It is mandatory to change this variable to a local ClamAV virus definition repository. Cisco limits the amount of pull from their virus definition repository so a local one should be setup to prevent rate limits.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.freshclam.checks` | This Variable sets how many times per day ClamAV checks for virus definition updates. | `"12"` | This can be changed based on organizational policy and must be a string, not an integer.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.freshclam.notify_clamd` | This Variable defines the ClamAV configuration to use when reloading the Daemon. | `/etc/clamd.d/scan.conf` | There should not be a need to change the default.
| Required only when `deploy_clamav` is `"yes"`. | `clamav_threat_prevention.freshclam.receive_timeout` | This Variable defines the timeout in attempting to communicate with the virus definition repository before failing. | `"60"` | This can be tweaked to accommodate for various network nuances and is a string value, not an integer.
| | `disable_storing_coredumps` | Disables the storing of Coredumps on the filesystem. | `"yes"` | Value must be a string. The only need to change this would be for troubleshooting Kernel level issue. If there is a requirement to do Kernel level debugging and development, you probably want this set to `"no"`.
| | `disable_coredump_backtrace` | Disables the Coredump Backtrace. | `"yes"` | Value must be a string. The only need to change this would be for troubleshooting Kernel level issue. If there is a requirement to do Kernel level debugging and development, you probably want this set to `"no"`.
| | `configure_selinux_mode` | Determine whether to configure SELinux mode or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `selinux_mode` | Sets the SELinux mode. | `"enforcing"` | Value must be a string. VAlid options are `"enforcing"`, `"permissive"`, and `"disabled"`.
| | `configure_aide_cron_job` | Determine whether to configure AIDE Daily Cron job or not. | `"yes"` | Value must be a string. Valid options are `"yes"` and `"no"`.
| | `aide_daily_schedule` | Set the AIDE daily Cron job schedule time. | `"30 04 * * *"` | Value must be a string. Value can be changed based on organization policy and follows standard Cron formatting. Only required when changing the default value and if `configure_aide_cron_job` is set to `"yes"`.

Example Threat Prevention Variables
-----------------------------------

```yaml
---
deploy_mcafee: "yes"
mcafee_threat_prevention:
  packages:
    - mcafeetp
  daemon:
    - mfetpd

deploy_clamav: "no"
clamav_threat_prevention:
  packages:
    - clamav
    - clamd
    - clamav-update
  daemon:
    - clamd@scan
    - freshclam
  freshclam:
    log_file: /var/log/freshclam.log
    private_mirror: changeme.net
    checks: "12"
    notify_clamd: /etc/clamd.d/scan.conf
    receive_timeout: "60"
    db_owner: clamupdate
```

Example Playbook
----------------

The general usage of this Ansible Role will fall in line with the usage of most typical Ansible Roles. Below is an example Playbook that can be used for testing.

```yaml
---
- name: STIG | RHEL 9 testing Role
  hosts: servers
  become: true
  tasks:
    - name: STIG | RHEL 9 import rhel_9_disa_stig Role
      ansible.builtin.import_role:
        name: rhel_9_disa_stig
```

License
-------

Copyright 2024 John Wynn

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
