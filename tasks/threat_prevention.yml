---
- name: THREAT_PREVENTION | RHEL 9 Block to configure McAfee
  when: 'deploy_mcafee == "yes"'
  block:
    - name: THREAT_PREVENTION | RHEL 9 install and enable McAfee Linux Threat Prevention tool
      ansible.builtin.dnf:
        name: "{{ mcafee_threat_prevention.packages }}"
        state: present
    - name: THREAT_PREVENTION | RHEL 9 ensure McAfee daemon is running and enabled
      ansible.builtin.service:
        name: "{{ mcafee_threat_prevention.daemon }}"
        state: started
        enabled: true

- name: THREAT_PREVENTION | RHEL 9 Block to configure ClamAV
  when: 'deploy_clamav == "yes"'
  block:
    - name: THREAT_PREVENTION | RHEL 9 install and enable ClamAV Linux Threat Prevention tool
      ansible.builtin.dnf:
        name: "{{ clamav_threat_prevention.packages }}"
        state: present
      notify: Freshclam

    - name: THREAT_PREVENTION | RHEL 9 create ClamAV log directory
      ansible.builtin.file:
        path: /var/log/clamav
        state: directory
        seuser: system_u
        setype: antivirus_log_t
        owner: clamupdate
        group: clamupdate
        mode: '0755'

    - name: THREAT_PREVENTION | RHEL 9 create ClamAV log file
      ansible.builtin.file:
        path: /var/log/clamav/freshclam.log
        state: touch
        modification_time: preserve
        access_time: preserve
        seuser: system_u
        setype: antivirus_log_t
        owner: clamupdate
        group: clamupdate
        mode: '0640'

    - name: THREAT_PREVENTION | RHEL 9 set antivirus_can_scan_system SELinux Policy for ClamAV
      ansible.posix.seboolean:
        name: antivirus_can_scan_system
        state: true
        persistent: true

    - name: THREAT_PREVENTION | RHEL 9 set clamd_use_jit SELinux Policy for ClamAV
      ansible.posix.seboolean:
        name: clamd_use_jit
        state: true
        persistent: true

    - name: THREAT_PREVENTION | RHEL 9 push ClamAV Freshclam SystemD unit file
      ansible.builtin.template:
        src: templates/freshclam.service.j2
        dest: /usr/lib/systemd/system/freshclam.service
        owner: root
        group: root
        mode: '0644'

    - name: THREAT_PREVENTION | RHEL 9 push ClamAV Freshclam configuration file
      ansible.builtin.template:
        src: templates/freshclam.conf.j2
        dest: /etc/freshclam.conf
        owner: root
        group: root
        mode: '0600'

    - name: THREAT_PREVENTION | RHEL 9 push ClamAV configuration file
      ansible.builtin.template:
        src: templates/scan.conf.j2
        dest: /etc/clamd.d/scan.conf
        owner: root
        group: root
        mode: '0644'

    - name: THREAT_PREVENTION | RHEL 9 flush Handlers to perform Freshclam action
      ansible.builtin.meta: flush_handlers

    - name: THREAT_PREVENTION | RHEL 9 ensure ClamAV daemons are running and enabled
      ansible.builtin.service:
        name: "{{ item }}"
        state: started
        enabled: true
      loop: "{{ clamav_threat_prevention.daemon }}"

- name: THREAT_PREVENTION | RHEL 9 push Coredump configuration file
  ansible.builtin.template:
    src: templates/coredump.conf.j2
    dest: /etc/systemd/coredump.conf
    trim_blocks: false
    owner: root
    group: root
    mode: '0644'

- name: THREAT_PREVENTION | RHEL 9 ensure SELinux mode is set to {{ selinux_mode }}
  ansible.posix.selinux:
    policy: targeted
    state: "{{ selinux_mode }}"
  when: 'configure_selinux_mode == "yes"'
  register: selinux_status
  notify:
    - Reboot message

- name: THREAT_PREVENTION | RHEL 9 apply SELinux context to nondefault faillock tally directory
  community.general.sefcontext:
    target: "/var/log/faillock(/.*)?"
    setype: faillog_t
    state: present
  when: 'enable_faillock_persist == "yes"'

- name: THREAT_PREVENTION | RHEL 9 configure AIDE daily cronjob
  when: 'configure_aide_cron_job == "yes"'
  block:
    - name: THREAT_PREVENTION | RHEL 9 push AIDE cron job to system
      ansible.builtin.template:
        src: templates/aide.j2
        dest: /etc/cron.daily/aide
        owner: root
        group: root
        mode: '0755'

    - name: THREAT_PREVENTION | RHEL 9 configure crontab to run AIDE cron job
      ansible.builtin.lineinfile:
        path: /etc/crontab
        insertafter: '# \*.+'
        line: '{{ aide_daily_schedule }} root usr/sbin/aide'
        owner: root
        group: root
        mode: '0600'

    - name: THREAT_PREVENTION | RHEL 9 configure AIDE cron for spool
      ansible.builtin.lineinfile:
        path: /var/spool/cron/root
        line: '{{ aide_daily_schedule }} root usr/sbin/aide'
        create: true
        owner: root
        group: root
        mode: '0600'
